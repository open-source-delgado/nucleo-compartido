﻿using MediatR;
namespace Delgado.NucleoCompartido;

public abstract class EventoBase: INotification
{
    /// <summary>
    /// Indica si este evento debe ser publicado fuera del contexto actual
    /// <para>Valor predeterminado = false</para>
    /// </summary>
    public bool PublicarMensaje { get; protected set; } = false;

    /// <summary>
    /// Fecha UTC en la que se ha creado el evento
    /// <para>Valor predeterminado = DateTimeOffset.UtcNow</para>
    /// </summary>
    public DateTimeOffset FechaUtcDelEvento { get; protected set; } = DateTimeOffset.UtcNow;
    
    /// <summary>
    /// Nombre del evento
    /// </summary>
    public string NombreDelEvento { get; protected set; }

    /// <summary>
    /// El Id del evento.
    /// <para>Valor predeterminado = Guid.NewGuid()</para>
    /// </summary>
    public Guid Id { get; protected set; } = Guid.NewGuid();
}
