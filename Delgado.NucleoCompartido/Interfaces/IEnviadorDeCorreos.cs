﻿namespace Delgado.NucleoCompartido.Interfaces;

public interface IEnviadorDeCorreos
{
    /// <summary>
    /// Envia un correo electronico de forma asincrona.
    /// TODO: Quizas este va en el Nucleo Compartido para ser usado por otros contextos
    /// </summary>
    /// <param name="destinatario">El correo electronico de la persona que recibira el correo</param>
    /// <param name="remitente">El correo electronico de la persona que envia el correo</param>
    /// <param name="tema">El tema o motivo del mensaje</param>
    /// <param name="contenido">El contenido del correo electronico</param>
    Task EnviarCorreoAsync(string destinatario, string remitente, string tema, string contenido);
}
