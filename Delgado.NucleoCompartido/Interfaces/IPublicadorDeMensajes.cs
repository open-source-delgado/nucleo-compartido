﻿namespace Delgado.NucleoCompartido.Interfaces;

/// <summary>
/// Interface usada para publicar mensajes a otros contextos
/// <para>Esta utiliza un <see cref="EventoBase"/> de integracion</para>
/// </summary>
public interface IPublicadorDeMensajes
{
    void Publicar(EventoBase evento);
}
