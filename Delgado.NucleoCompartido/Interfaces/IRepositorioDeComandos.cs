﻿namespace Delgado.NucleoCompartido.Interfaces;

/// <summary>
/// Interface definiendo funcionalidad para comandos para crear, modificar y eliminar registros de una base de datos
/// <para>
/// Utilizado como el Command en el CQRS (Command and Query Responsibility Segregation pattern)
/// </para>
/// </summary>
/// <typeparam name="T">T representa el objeto agregado</typeparam>
public interface IRepositorioDeComandos<T> where T : class, IRaizAgregada
{
    Task<T> Agregar(T entidad, CancellationToken tokenDeCancelacion = default);
    Task Actualizar(T entidad, CancellationToken tokenDeCancelacion = default);
    Task Eliminar(T entidad, CancellationToken tokenDeCancelacion = default);
    Task EliminarVarios(IEnumerable<T> entidades, CancellationToken tokenDeCancelacion = default);
    Task<int> SalvarCambios(CancellationToken tokenDeCancelacion = default);
}